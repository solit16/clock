onload  = () => {
	const canvas = document.getElementById('canvas');
	const ctx = canvas.getContext('2d');
	canvas.width = 500;
	canvas.height = 500;
	ctx.lineWidth = 8;
	ctx.lineCap = "round";
	ctx.fillStyle = "red";
	ctx.strokeStyle = "green";

	const face = () => {
		ctx.setTransform();
		ctx.fillStyle = "black";
		ctx.fillRect(0, 0, canvas.width, canvas.height);
		ctx.translate(250, 250)

		ctx.beginPath()
		for (let i = 0; i < 12; i++) {
    		ctx.rotate(Math.PI/6); 
    		ctx.moveTo(200, 0);
    		ctx.lineTo(220, 0);
    		ctx.stroke();
		}
	
	}
	
	
	const draw = t => {
		face();
		const sin_sec = Math.sin(t/360);
		const cos_sec = Math.cos(t/360);

		ctx.fillStyle = "white";
		ctx.setTransform(sin_sec, -cos_sec, cos_sec, sin_sec, canvas.width / 2, canvas.height / 2);

		for (let i = 0; i < 1; i++) {
			ctx.fillRect(0, 0, 230, 3, -5*i);
			ctx.transform(1, 0, 0, 1, 50, 1);
		}

		const sin_min = Math.sin(t/4320);
		const cos_min = Math.cos(t/4320);

		ctx.fillStyle = "yellow";
		ctx.setTransform(sin_min, -cos_min, cos_min, sin_min, canvas.width / 2, canvas.height / 2);

		for (let i = 0; i < 1; i++) {
			ctx.fillRect(0, 0, 170, 3, -5*i);
			ctx.transform(1, 0, 0, 1, 50, 1);
		}
		const sin_h = Math.sin(t/51840);
		const cos_h = Math.cos(t/51840);

		ctx.fillStyle = "red";
		ctx.setTransform(sin_h, -cos_h, cos_h, sin_h, canvas.width / 2, canvas.height / 2);

		for (let i = 0; i < 1; i++) {
			ctx.fillRect(0, 0, 120, 3, -5*i);
			ctx.transform(1, 0, 0, 1, 50, 1);
		}
		requestAnimationFrame(draw);

	}
	requestAnimationFrame(draw);
}
